from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import sys, math

cities=["ahmedabad","bangalore","chennai","delhi","ghaziabad","greater-noida","gurgaon","hyderabad","kolkata","mumbai","noida","pune","bhubaneswar","coimbatore","faridabad","jaipur","lucknow","mangalore","mysore","nagpur","nashik","trivandrum","visakhapatnam"]

browser = None
try:
	firefox_capabilities = DesiredCapabilities.FIREFOX
	firefox_capabilities['marionette'] = True
	firefox_capabilities['binary'] = '/usr/bin/firefox'
	browser = webdriver.Firefox()
except Exception as error:
	print(error)

class PropListGen:
	def __init__(self, city):
		self.url = "https://www.proptiger.com/{0}/property-sale".format(city)
		self.html_text = None
		self.file = "{}_prop_details.txt".format(city)
		#print(self.url)
		try:
			browser.get(self.url) 
			self.html_text = browser.page_source
		except Exception as err:
			print(self(err))
			return

		self.soup = None
		self.pages = 1
		if self.html_text is not None:
			self.soup = BeautifulSoup(self.html_text, 'lxml')
			abc=self.soup.find('li',attrs={'class': 'total-projects'})
			self.pages = math.ceil(int(abc.find('span', attrs={'class': 'bigtxt'}).string)/15.0)
		else:
			print("Page is empty")

	def scrap_page(self, page):
		page_url=self.url+"?page="+str(page)
		page_html= None
		try:
			browser.get(page_url) 
			page_html = browser.page_source
		except Exception as e:
			print(self(e))
			return
		if page_html is not None:
			s= BeautifulSoup(page_html, 'lxml')
			divs = s.find_all('div', attrs={'class': 'project-card-info-wrap'})
			links=[]
			for div in divs:
				links.append("https://www.proptiger.com"+str(div['data-url']))
			print("Page",page,"done")
			return links
		else:
			print("Error in page",page)
			return []

	def scrap(self):
		out_file = open(self.file,"w")
		data=[]
		for i in range(1,self.pages+1):
			l=self.scrap_page(i)
			data+=l
		print(data)
		out_file.write('\n'.join(data))
		out_file.close()

if __name__ == '__main__':
	if browser is None:
		print("Selenium not opened")
		sys.exit()
	for city in cities:
		a=PropListGen(city)
		a.scrap()

	if browser is not None:
		browser.quit()
#for city in cities:

