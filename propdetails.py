from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import sys,json
from urllib.parse import urlparse
import urllib.request

browser = None
try:
	firefox_capabilities = DesiredCapabilities.FIREFOX
	firefox_capabilities['marionette'] = True
	firefox_capabilities['binary'] = '/usr/bin/firefox'
	browser = webdriver.Firefox()
except Exception as error:
	print(error)

class Property:
	def __init__(self, url):
		self.url = url
		self.html_text = None
		try:
			browser.get(self.url) 
			self.html_text = browser.page_source
		except Exception as err:
			print(self(err))
			return

		self.soup = None
		if self.html_text is not None:
			self.soup = BeautifulSoup(self.html_text, 'lxml')

	def scrap(self):
		if self.soup is None:
			return {}
		soup = self.soup
		detail = dict()
		a=soup.find('div',attrs={'id': 'projectDetailsPageWrapperFull'})
		if a is None:
			print('Project Does not exist anymore')
			return None
		if a['data-title']!=None:
			detail["title"]=a['data-title']
		else:
			detail["title"]=None
		if a['data-builderid']!=None:
			detail["builderid"]=a['data-builderid']
		else:
			detail["builderid"]=None
		if a['data-projectid']!=None:
			detail["projectid"]=a['data-projectid']
		else:
			detail["projectid"]=None
		if a['data-localityid']!=None:
			detail["localityid"]=a['data-localityid']
		else:
			detail["localityid"]=None
		if a['data-unittype']!=None:
			detail["unittype"]=a['data-unittype']
		else:
			detail["unittype"]=None
		if a['data-lat']!=None and a['data-long']!=None:
			detail["geo"]={"lat": a['data-lat'], "lon": a['data-long']}
		else:
			detail["geo"]=None
		if a['data-cityname']!=None:
			detail["cityname"]=a['data-cityname']
		else:
			detail["cityname"]=None
		if a['data-projlabel']!=None:
			detail["projlabel"]=a['data-projlabel']
		else:
			detail["projlabel"]=None
		if a['data-localitylabel']!=None:
			detail["localitylabel"]=a['data-localitylabel']
		else:
			detail["localitylabel"]=None
		if a['data-projectname']!=None:
			detail["projectname"]=a['data-projectname']
		else:
			detail["projectname"]=None
		if a['data-budgetlowerlimit']!=None:
			detail["budgetlowerlimit"]=a['data-budgetlowerlimit']
		else:
			detail["budgetlowerlimit"]=None
		if a['data-budgetupperlimit']!=None:
			detail["budgetupperlimit"]=a['data-budgetupperlimit']
		else:
			detail["budgetupperlimit"]=None
		if a['data-distinctbedrooms']!=None:
			detail["distinctbedrooms"]=a['data-distinctbedrooms']
		else:
			detail["distinctbedrooms"]=None
		if a['data-builder']!=None:
			detail["builder"]=a['data-builder']
		else:
			detail["builder"]=None
		if a['data-builderdisplayname']!=None:
			detail["builderdisplayname"]=a['data-builderdisplayname']
		else:
			detail["builderdisplayname"]=None
		if a['data-projectlivability']!=None:
			detail["projectlivability"]=a['data-projectlivability']
		else:
			detail["projectlivability"]=None
		a = soup.find('ul', attrs = {'class': 'icons-amenities-list'})
		if a!=None:
			detail["amenities"]=[]
			lis= a.find_all('div', attrs={'class': 'amenity-name'})
			for li in lis:
				if li is not None: detail["amenities"].append(li.string)
		else:
			detail["amenities"]=None
		a = soup.find('div', attrs={'class': 'project-spec-wrap'})
		a=a.find_all('div', attrs={'class': 'spec-section'})[1].find_all('div', attrs={'class': 'project-spec'})[1]
		if a is not None:
			detail["arearange"]=[]
			x,y=a.find('span', attrs={'class', 'first-val'}), a.find('span', attrs={'class': 'second-val'})
			if x is not None:
				detail["arearange"].append(x.string)
			if y is not None:
				detail["arearange"].append(y.string)
		else:
			detail["arearange"]=None
		a = soup.find('ul', attrs = {'class': 'config-range'})
		if a is not None:
			sizevsprice=dict()
			lis=a.find_all('li')
			for i in range(len(lis)):
				lis[i]=lis[i].string
			a = soup.find_all('div', attrs={'class': 'config-table-wrap'})
			if a is not None and len(lis)==len(a):
				for i in range(len(a)):
					bhk=dict()
					div=a[i]
					tr=div.find_all('tr', attrs={'itemscope': 'itemscope'})
					for row in tr:
						k = row.find_all('div', attrs={'class': 'size'})
						bhk[k[0].find('a').string]= k[1].string.strip()
					sizevsprice[lis[i]]=bhk
			detail["floorplan"]=sizevsprice
		else:
			detail["floorplan"]=None
		return detail


if __name__ == '__main__':
	if browser is None:
		sys.exit()

	#a=Property('https://www.proptiger.com/ahmedabad/near-nirma-university-on-sg-highway/godrej-properties-garden-city-501448')
	#print(a.scrap())

	cities=["ahmedabad", "bangalore","chennai","delhi","ghaziabad","greater-noida","gurgaon","hyderabad","kolkata","mumbai","noida","pune","bhubaneswar","coimbatore","faridabad","jaipur","lucknow","mangalore","mysore","nagpur","nashik","trivandrum","visakhapatnam"]
	for city in cities:
		try:
			f=open("{0}_prop_details.txt".format(city), 'r')
			out_file = open("property_{0}".format(city), 'a')
			props=f.readlines()
			for prop in props:
				prop=prop[:-1]
				print("Trying url: {0}".format(prop))
				a=Property(prop)
				try:
					json.dump(a.scrap(), out_file)
					out_file.write('\n')
				except Exception as e:
					print(e)
			out_file.close()
		except Exception as err:
			print (err)
			
	if browser is not None:
		browser.quit()